package com.globalvox.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.globalvox.model.City;

@Repository
public interface CityRepository extends CrudRepository<City, Integer> {
	
	@Query(value = "select (temp_min + temp_max) / 2 as avg_temp from CITY_MASTER where lower(city_name) = lower(:cityName)", nativeQuery=true)
    public BigDecimal findAvgTempByCity(String cityName);

}
