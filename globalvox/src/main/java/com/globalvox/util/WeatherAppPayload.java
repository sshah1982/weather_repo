package com.globalvox.util;

import java.io.Serializable;
import java.util.Date;

import org.springframework.http.HttpStatus;

public class WeatherAppPayload implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int errorCode;
	
	private HttpStatus httpStatus;
	
	private Date date;
	
	private String message;
	
	private Object data;

	public WeatherAppPayload() {
		super();
		// TODO Auto-generated constructor stub
		
	}

	public WeatherAppPayload(int errorCode, HttpStatus httpStatus, Date date, String message, Object data) {
		super();
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
		this.date = date;
		this.message = message;
		this.data = data;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
	
	

}
