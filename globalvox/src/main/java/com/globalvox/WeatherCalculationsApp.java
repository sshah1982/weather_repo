package com.globalvox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherCalculationsApp {

	public static void main(String[] args) {
		SpringApplication.run(WeatherCalculationsApp.class, args);
	}

}
