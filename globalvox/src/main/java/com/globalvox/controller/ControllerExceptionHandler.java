package com.globalvox.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.globalvox.util.WeatherAppConstants;
import com.globalvox.util.WeatherAppPayload;

@RestControllerAdvice
public class ControllerExceptionHandler {

	@ExceptionHandler(value = { FileNotFoundException.class, IOException.class })
	public WeatherAppPayload fileNotFoundException(FileNotFoundException ex, WebRequest request) {
		WeatherAppPayload message = new WeatherAppPayload(WeatherAppConstants.FAILURE, 
				HttpStatus.INTERNAL_SERVER_ERROR, new Date(), ex.getMessage(), null);

		return message;
	}
}
