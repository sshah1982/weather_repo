package com.globalvox.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globalvox.model.City;
import com.globalvox.model.CityList;
import com.globalvox.model.WeatherInfo;
import com.globalvox.service.CityService;
import com.globalvox.util.WeatherAppConstants;
import com.globalvox.util.WeatherAppPayload;
import com.google.gson.Gson;

@RestController
@RequestMapping("/city")
public class CityController  {
	
	@Autowired
	private CityService cityService;

	@PostMapping(value = "/insertBulk", produces = "application/json")
	public WeatherAppPayload insertCities() {
		File resource = null;
		try {
			resource = new ClassPathResource(
				      "Weather.json").getFile();
			
			Gson gson = new Gson();
			BufferedReader br = new BufferedReader(new FileReader(resource));
			CityList cityLst = gson.fromJson(br,  CityList.class);
			//System.out.println(cityLst.getCities().size());
			
			try {
				cityService.saveAll(cityLst.getCities());
			}
			catch(ConstraintViolationException ex) {
				return new WeatherAppPayload(WeatherAppConstants.FAILURE, 
						HttpStatus.CONFLICT, new Date(), ex.getMessage(), null);
			}
			
			return new WeatherAppPayload(WeatherAppConstants.SUCCESS, 
					HttpStatus.OK, new Date(), "Cities inserted successfully", null);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new WeatherAppPayload(WeatherAppConstants.FAILURE, 
					HttpStatus.INTERNAL_SERVER_ERROR, new Date(), e.getMessage(), null);
		}
	}
	
	@GetMapping(value = "/getAll", produces = "application/json")
	public WeatherAppPayload getAllCities() {
		List<City> cityList = cityService.getAll();
		
		return new WeatherAppPayload(WeatherAppConstants.SUCCESS, 
				HttpStatus.OK, new Date(), "", cityList);
	}
	
	@GetMapping(value = "/getCityWeatherInfo/{city}", produces = "application/json")
	public WeatherAppPayload getCityWeatherInfo(@PathVariable("city") String city) {
		
		WeatherInfo info = cityService.getCityWeatherInfo(city);
		
		if(info == null) {
			return new WeatherAppPayload(WeatherAppConstants.FAILURE, 
					HttpStatus.NOT_FOUND, new Date(), "City not found", null);
		}
		else {
			return new WeatherAppPayload(WeatherAppConstants.SUCCESS, 
					HttpStatus.OK, new Date(), "", info);
		
		}
		
	}
}
