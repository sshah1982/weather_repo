package com.globalvox.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Service;

import com.globalvox.model.City;
import com.globalvox.model.WeatherInfo;

@Service
public interface CityService {
	public void saveAll(List<City> cityList) throws ConstraintViolationException;
	
	public List<City> getAll();
	
	public WeatherInfo getCityWeatherInfo(String city);
	
}
