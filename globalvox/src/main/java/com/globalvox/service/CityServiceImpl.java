package com.globalvox.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalvox.model.City;
import com.globalvox.model.WeatherInfo;
import com.globalvox.repository.CityRepository;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	
	@Autowired
	private CityRepository cityRepo;

	@Override
	public void saveAll(List<City> cityLst) throws ConstraintViolationException {
		// TODO Auto-generated method stub
		cityRepo.saveAll(cityLst);
	}

	@Override
	public List<City> getAll() {
		// TODO Auto-generated method stub
		Iterable<City> it = cityRepo.findAll();
		
		Iterator<City> cityIt = it.iterator();
	
		List<City> cityLst = new ArrayList<>();
		while(cityIt.hasNext()) {
			cityLst.add(cityIt.next());
		}
		
		return cityLst;
	}

	@Override
	public WeatherInfo getCityWeatherInfo(String city) {
		// TODO Auto-generated method stub
		BigDecimal avgTemp = cityRepo.findAvgTempByCity(city);
		
		if(avgTemp == null) {
			return null;
		}
		
		WeatherInfo info = new WeatherInfo();
		info.setCity(city);
		info.setAvgTemp(avgTemp);
		
		return info;
	}
	
	

}
