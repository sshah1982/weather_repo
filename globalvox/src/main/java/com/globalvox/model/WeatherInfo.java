package com.globalvox.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class WeatherInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String city;
	
	private BigDecimal avgTemp;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public BigDecimal getAvgTemp() {
		return avgTemp;
	}

	public void setAvgTemp(BigDecimal avgTemp) {
		this.avgTemp = avgTemp;
	}
	
	

}
