package com.globalvox.controller.tests;

import static org.junit.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.globalvox.AbstractTest;
import com.globalvox.model.City;
import com.globalvox.util.WeatherAppPayload;

public class CityControllerTest extends AbstractTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void insertCitiesTest_Success() throws Exception {
		String uri = "/city/insertBulk";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		String content = mvcResult.getResponse().getContentAsString();
		WeatherAppPayload response = super.mapFromJson(content, WeatherAppPayload.class);
		assertTrue(response.getMessage().equals("Cities inserted successfully"));
	}

	@Test
	public void getAllCitiesTest_Success() throws Exception {
		String uri = "/city/getAll";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		String content = mvcResult.getResponse().getContentAsString();
		WeatherAppPayload response = super.mapFromJson(content, WeatherAppPayload.class);
		assertTrue(((List<City>) response.getData()).size() > 0);
	}
	
	@Test
	public void getCityWeatherInfoTest_Success() throws Exception {
		String uri = "/city/getCityWeatherInfo/protaras";
		
		String city = "protaras";

		String inputJson = super.mapToJson(city);

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(inputJson)).andReturn();

		String content = mvcResult.getResponse().getContentAsString();
		WeatherAppPayload response = super.mapFromJson(content, WeatherAppPayload.class);
		
		assertTrue(((LinkedHashMap)response.getData()).get("city").equals("protaras"));
	}
	
	@Test
	public void getCityWeatherInfoTest_Failure() throws Exception {
		String uri = "/city/getCityWeatherInfo/abc";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		String content = mvcResult.getResponse().getContentAsString();
		WeatherAppPayload response = super.mapFromJson(content, WeatherAppPayload.class);
		assertTrue(response.getMessage().equals("City not found"));
		
	}



}
